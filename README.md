Lucky’s Music Box is a web application artifact that combines computers, cats, and music. The web application has three features: 
a curated Spotify playlist generator, a piano keyboard with cat meows, and an area to “meet Lucky’s friends”, which
lists cats available for adoption around the user’s location. The curated Spotify playlist generates a playlist based on the user’s
current weather conditions. To further personalize each playlist to the user, the user’s top artist or preferred artist, the user’s
top song, and the seed genres associated with the current weather condition also play a part in crafting a unique playlist for
each user. The piano keyboard with cat meows combines the love for music and cats. The piano has 13 “keys” controlled
through the keys on a computer’s keyboard. The meows are recorded from real cats and featured in the “Meet Lucky’s
friends” area. Lucky’s music box was developed to explore the theme of computer music but also to
encourage cat and music lovers to have fun.

To run our React application, within the terminal, run 'npm start'

When the application opens, you will be greeted with our home page and a button that links to spotify's login. 

*** In order test the full functionality of our application, the email associated to the spotify account must be registered to our Spotify
for developers account. We made an account for you to test with: ***

Username/Email: CreativeComputing2023@gmail.com
Password: CreativeComputing2023!