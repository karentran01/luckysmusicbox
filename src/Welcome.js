import React from "react";
import {Container} from "@mui/system";
import "./Welcome.css"
import {loginUrl} from "./spotify";

function Welcome() {
    return (
        <Container className="App">
                <div className="centered">
                    <div id="scaled">
                        <h2 id="welcome">Welcome to</h2>
                        <h2 id="title">Lucky's Music Box</h2>
                        <br/>
                        <img alt="lucky" src="/images/welcome/Lucky.svg"/>
                        <br/>
                        <br/>
                        <img alt="track" src="/images/welcome/Track.svg"/>
                        <br/>
                        <img alt="play" src="/images/welcome/Play.svg"/>
                        <br/>
                        <br/>
                        <br/>
                        <a href={loginUrl} id="signinButton"> Sign in with Spotify to continue</a>
                    </div >
                </div>
        </Container>
    );
}

export default Welcome;
