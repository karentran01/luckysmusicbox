import React from "react";
import {Box, Container} from "@mui/system";

import "./FriendDetailPage.css"

const FriendDetailPage = (props) => {
    const init = () => {

    }

    init();

    return (
        <Container className="App">
            <div style={{display: "flex"}}>
                <div>
                    <img src={`/images/meetluckysfriends/friends/Blue.png`} />

                    <div style={{backgroundColor:"#FFFFFF", borderRadius:"8px", padding: "4%", marginTop: "8%"}}>
                        <label>
                            Sex
                        </label>

                        <p className={"subheading"}>
                            Female
                        </p>
                        <label>
                            Age
                        </label>

                        <p className={"subheading"}>
                            1
                        </p>

                        <label>
                            Animal ID
                        </label>

                        <p className={"subheading"}>
                            12345
                        </p>

                        <label>
                            Location
                        </label>

                        <p className={"subheading"}>
                            Roanoke
                        </p>
                    </div>
                </div>
                <div style={{textAlign: "left", marginLeft: "2%"}}>
                    <h1>
                        Lucky
                    </h1>

                    <div style={{backgroundColor:"#FFFFFF", borderRadius:"8px", borderStyle: "solid", borderColor: "#6F3324", borderWidth: "1px"}}>
                        <p>
                            Meow! My name is Blue. I’m an elegant girl who loves to watch what is going on around me. I may be a little shy at first but once I get comfortable I am very friendly. I do love to play with my toys so come meet me. I am spayed, vaccinated, microchipped and combo tested. My adoption fee is $50.
                            https://rvspca.org/adoption-gallery/blue/
                        </p>

                        <div style={{textAlign: "right", paddingRight: "2%"}}>
                            <img src={`/images/miscellaneous/heart.svg`} />
                        </div>

                    </div>
                </div>
            </div>
        </Container>
    );
}

export default FriendDetailPage