import React from "react";
import {Box, Container} from "@mui/system";
import CatCardItem from './CatCardItem'

const MeetLuckysFriends = (props) =>  {
    const init = () => {
    }

    init();

    return (
        <Container className="App">
            <img alt="lucky" src="/images/meetluckysfriends/LuckyProPic.svg"/>
            <h1>Lucky's Friends</h1>
            <img alt="lucky" src="/images/meetluckysfriends/LuckyTextBlurb.svg"/>

            {
                props.petFinder.map((item, index) => {
                    if ((index + 1) % 3 === 0 && (props.petFinder !== null || props.petFinder !== undefined)) {
                        return (
                            <>
                                <Box sx={{mt: '2px', display: 'grid', gridTemplateColumns: 'repeat(3, 1fr)', backgroundColor: '#FBD7C4'}}>
                                    <CatCardItem url={props.petFinder[index].url} profile={props.petFinder[index].primary_photo_cropped} name = {props.petFinder[index].name.toLowerCase()} gender = {props.petFinder[index].gender.toLowerCase()} age = {props.petFinder[index].age.toLowerCase()} animalId = {props.petFinder[index].id}></CatCardItem>
                                    <CatCardItem url={props.petFinder[index + 1].url} profile={props.petFinder[index + 1].primary_photo_cropped} name = {props.petFinder[index + 1].name.toLowerCase()} gender = {props.petFinder[index + 1].gender.toLowerCase()} age = {props.petFinder[index + 1].age.toLowerCase()} animalId = {props.petFinder[index + 1].id}></CatCardItem>
                                    <CatCardItem url={props.petFinder[index + 2].url} profile={props.petFinder[index + 2].primary_photo_cropped} name = {props.petFinder[index + 2].name.toLowerCase()} gender = {props.petFinder[index + 2].gender.toLowerCase()} age = {props.petFinder[index + 2].age.toLowerCase()} animalId = {props.petFinder[index + 2].id}></CatCardItem>
                                </Box>
                            </>
                        )
                    } else {

                    }

                    }
                )
            }
        </Container>

    );
}

export default MeetLuckysFriends