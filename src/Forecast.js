import React, { useEffect, useState } from "react";
import Weather from './Weather';
import SpotifyPlayerComp from "./SpotifyPlayer"
import {Box} from "@mui/system";
import "./Forecast.css"
import TextField from "@mui/material/TextField"
import MeetLuckysFriends from "./MeetLuckysFriends";
import Button from "@mui/material/Button"
import { Client } from "@petfinder/petfinder-js";

/**
 * The APIs used within this project include: OpenWeatherAPI, SpotifyAPI, and PetfinderAPI.
 */

// Key for the PetfinderAPI
const client = new Client({apiKey: "P07ZOQJCWrCXepFyipcEzrnUXT0wzqf7V4CLVHDs4teasM7Kl7", secret: "FlNYqp9fzgYHJ4TFHZ3QJFzx2Kf612ABiPDKysyV"});

// ket for OpenWeatherAPI
const key = "ff891d83e0f15a60da8709b967782f25";
const url = "https://api.openweathermap.org/data/2.5"

const Forecast = ({spotify_token}) =>  {
    const [lat, setLat] = useState([]);
    const [long, setLong] = useState([]);
    const [location, setLocation] = useState([]);
    const [playlist, setPlaylist] = useState('')
    const [typeOfCat, setTypeOfCat] = useState('tan');
    const [swatCat, setSwatCat] = useState('swat2.png' );

    const [petfinder, setPetfinder] = useState([]);

    const [mood] = useState([{},
        {},
        {id: 2, seed_genres: ["metal", "dub", "dubstep", "edm", "metal-misc", "heavy-metal", "grunge","black-metal","death-metal","hard-rock", "hardcore", "metalcore", "post-dubstep"], cat: "black"},
        {id: 3, seed_genres: ["r-n-b", "acoustic", "guitar", "soul", "classical", "sleep","bossanova","emo","piano", "reggae"], cat: "black"},
        {},
        {id: 5, seed_genres: ["blues","rock", "rock-n-roll", "rockabilly", "jazz", "sad","punk", "punk-rock", "rainy-day", "alt-rock"], cat: "black"},
        {id: 6, seed_genres: ["holidays", "happy"], cat: "black"},
        {id: 7, seed_genres: ["road-trip", "summer", "country", "disco", "folk"], cat: "tan"},
        {id: 8, seed_genres: ["pop", "afrobeat", "spanish", "party", "ambient", "hip-hop", "romance", "disney", "latin", "reggaeton"], cat: "orange"},
        {id: 9, seed_genres: ["progressive-house", "chill", "indie", "indie-pop", "dance", "minimal-techno", "house"], cat: "orange"}])

    let artist = "";

    const goToLuckysFriends = () => {
        window.location.replace(window.location.pathname + 'meet-luckys-friends')
    }

    const getWeatherCondition = async () => {
        navigator.geolocation.getCurrentPosition(function(position) {
            setLat(position.coords.latitude);
            setLong(position.coords.longitude);
        });

        // Using the OpenWeatherAPI here
        let weatherData = "";
        await fetch(`${url}/weather/?lat=${lat}&lon=${long}&units=metric&APPID=${key}`)
            .then(res => res.json())
            .then(result => {
                setLocation(result);
                weatherData = result;
            }).catch(
                getWeatherCondition
            );

        let id = Math.ceil(weatherData.weather[0]['id']/100);
        console.log(id)
        setTypeOfCat(mood[id]['cat'])
        return id;
    }

    /**
     * return the users ID (ex: karentran01)
     * uses the SpotifyAPI
     * @returns {Promise<*>}
     */
    const getArtistID = async () => {
        let response = await fetch("https://api.spotify.com/v1/me", {
            headers: {
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${spotify_token}`
            }
        });
        let data = await response.json();
        let user_id = data.id;
        return user_id;
    }

    /**
     * return the artist from the textfield
     * uses the SpotifyAPI
     * @param event
     * @returns {Promise<void>}
     */
    const getArtistFromUser = async (event) => {
        artist = event.target.value
        let response = await fetch(`https://api.spotify.com/v1/search?q=${artist}&type=artist&limit=1`, {
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${spotify_token}`
            }
        });
        let data = await response.json();
        artist = data['artists']['items'][0]['id']
        await addSongstoPlaylist()
    }

    /**
     * return the user's top artist
     * uses the SpotifyAPI
     * @returns {Promise<*>}
     */
    const getUsersTopArtist = async () => {
        let response = await fetch("https://api.spotify.com/v1/me/top/artists?time_range=medium_term&limit=1&offset=0", {
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${spotify_token}`
            }
        });
        let data = await response.json();
        let seed_artist = (data.items[0]['uri']).split(":")[2];
        return seed_artist;
    }

    /**
     * return the user's top song
     * uses the SpotifyAPI
     * @returns {Promise<*>}
     */
    const getUsersTopSong = async () => {
        let response = await fetch("https://api.spotify.com/v1/me/top/tracks?time_range=medium_term&limit=1&offset=0", {
            headers: {
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${spotify_token}`
            }
        });
        let data = await response.json();
        let seed_track = (data.items[0]['uri']).split(":")[2];
        return seed_track
    }

    /**
     * return 15 songs based on the recommendations
     * uses the SpotifyAPI
     * @returns {Promise<any>}
     */
    const getRecommendedSongs = async () => {
        let weatherID = await getWeatherCondition();
        let seed_artists = await getUsersTopArtist();
        if (artist !== '') {
            seed_artists = artist;
        }
        let seed_tracks = await getUsersTopSong();
        let moods = mood[weatherID]['seed_genres'];

        let genre1 = Math.round(Math.random() * moods.length);
        let genre2 = Math.round(Math.random() * moods.length);

        console.log(genre1 + " " + genre2)
        let seed_genres = moods[genre1] + "%2C" + moods[genre2];
        // let seed_genres = "";
        // for (let i = 0; i < moods.length - 1; i++) {
        //     seed_genres = seed_genres + moods[i] + "%2C";
        // }
        // seed_genres = seed_genres + moods[moods.length - 1]

        const limit=15, market="US";
        // Get the limit of songs
        let query = 'https://api.spotify.com/v1/recommendations?limit=' + limit + '&market=' + market + '&seed_artists=' + seed_artists + '&seed_genres=' + seed_genres + '&seed_tracks=' + seed_tracks + '&target_danceability=0.1';
        let response = await fetch(query, {
            headers: {
                'Content-type' : 'application/json',
                'Authorization': `Bearer ${spotify_token}`
            }
        });
        let data = await response.json();

        let tracksToAdd = "?uris="
        if (data !== undefined) {
            for (let i = 0; i < data.tracks.length - 1; i++) {
                tracksToAdd += data.tracks[i].uri + "%2C"
            }
        }
        tracksToAdd = tracksToAdd + data.tracks[data.tracks.length - 1].uri;
        return tracksToAdd;
    }

    /**
     * return snapshot if the playlist with songs was created
     * uses the SpotifyAPI
     * @returns {Promise<void>}
     */
    const addSongstoPlaylist = async () => {
        let playlist_id = "";
        let user_id = await getArtistID();
        let tracksToAdd = await getRecommendedSongs()

        await fetch("https://api.spotify.com/v1/users/" + user_id + "/playlists", {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${spotify_token}`
            },
            body: JSON.stringify({
                "name": "Cat Playlist",
                "description": "testing catify",
                "public": true
            })
            })
            .then(res => res.json())
            .then(result => {
                playlist_id = result.id;
            });

        await fetch("https://api.spotify.com/v1/playlists/" + playlist_id + "/tracks" + tracksToAdd, {
             method: "POST",
             headers: {
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
                 'Authorization': `Bearer ${spotify_token}`
             },
             })
            .then(res => {res.json(); console.log(res)})
            .then(result => {
                console.log(result)
             });
        setPlaylist(playlist_id)
    }

    const useKeyPress = targetKey => {
        const [keyPressed, setKeyPressed] = React.useState(false);
        const downHandler = ({ key }) => {
            if (key === targetKey) {
                setKeyPressed(true);
            }
        };

        const upHandler = ({ key }) => {
            if (key === targetKey) {
                setKeyPressed(false);
            }
        };

        React.useEffect(() => {
            window.addEventListener('keydown', downHandler);
            window.addEventListener('keyup', upHandler);

            return () => {
                window.removeEventListener('keydown', downHandler);
                window.removeEventListener('keyup', upHandler);
            };
        }, []);

        return keyPressed;
    };

    useEffect(() => {
        const fetchData = async () => {
            await addSongstoPlaylist();
            navigator.geolocation.getCurrentPosition(function(position) {
                // uses the Petfinder API
                client.animal.search({
                    type: "Cat",
                    location: position.coords.latitude + "," + position.coords.longitude,
                    distance: 10})
                    .then(function (response) {
                        //console.log(response.data.animals)
                        setPetfinder(response.data.animals)
                    })
                    .catch(function (error) {
                        fetchData()
                    });
            });
        }
        fetchData()
    }, [lat, long])

    const aPressed = useKeyPress('a');
    const sPressed = useKeyPress('s');
    const dPressed = useKeyPress('d');
    const fPressed = useKeyPress('f');

    const jPressed = useKeyPress('j');
    const kPressed = useKeyPress('k');
    const lPressed = useKeyPress('l');
    const cPressed = useKeyPress(';');

    const wPressed = useKeyPress('w');
    const ePressed = useKeyPress('e');
    const iPressed = useKeyPress('i');
    const oPressed = useKeyPress('o');
    const pPressed = useKeyPress('p');

    const leftHand = aPressed || sPressed || dPressed || fPressed || wPressed || ePressed;
    const rightHand = jPressed || kPressed || lPressed || cPressed || iPressed || oPressed || pPressed;

    function changeBackground(e) {
        let x = document.getElementById("lucky-swatting")
        x.style.opacity =  '100%'
        const interval1 = setInterval(() => {
            setSwatCat("swat1.png");
        }, 1000);

        const interval2 = setInterval(() => {
            setSwatCat("swat2.png");
        }, 2000);
    }

    function changeBackgroundBack(e) {
        let x = document.getElementById("lucky-swatting")
        x.style.opacity =  '0%'
    }

    function changeBackground1(e) {
        let x = document.getElementById("lucky-mouse")
        x.style.opacity =  '100%'
        const interval1 = setInterval(() => {
            setSwatCat("swat1.png");
        }, 1000);

        const interval2 = setInterval(() => {
            setSwatCat("swat2.png");
        }, 1500);
    }

    function changeBackgroundBack1(e) {
        let x = document.getElementById("lucky-mouse")
        x.style.opacity =  '0%'
    }

    const toHelp = () => {
        window.location.replace(window.location.origin + "/help")
    }

    //<button onClick={goToLuckysFriends} id="meet-friends"> Meet Lucky's Friends </button>
    return (
        <div className="App">
            {(typeof location.main != 'undefined') ? (
                <container>
                    <Box display="grid" gridTemplateColumns="repeat(12, 1fr)">
                        <Box gridColumn="span 4">
                            <div className="sticky">
                                <Button id="help" onClick={toHelp}> ? </Button>
                                <div className="spotify-player">
                                    <Weather weatherData={location}/>
                                    <br/>
                                    <TextField sx={{"& fieldset": { border: 'none' },}} className="inputRounded" label="Have an artist you want us to consider?" size="small" fullWidth onBlur={getArtistFromUser}/>
                                    <SpotifyPlayerComp playlist_id={playlist}></SpotifyPlayerComp>
                                </div>

                                <div className="outer">
                                    <div className="mouse-cat-1">
                                        <img id="lucky-mouse" style={{opacity:"0%"}} alt="lucky" src={swatCat} />
                                    </div>
                                    <div className="mouse-cat-2">
                                        <img alt="lucky" src='mouse.png' className="mouse" onMouseOver={changeBackground1} onMouseOut={changeBackgroundBack1}/>
                                    </div>
                                </div>

                                <div className="outer">
                                    <div className="swat-cat-1">
                                        <img id="lucky-swatting" style={{opacity:"0%"}} alt="lucky" src={swatCat} />
                                    </div>
                                    <div className="swat-cat-2">
                                        <img alt="luckyasf" src='swat-1.png' className="swat" onMouseOver={changeBackground} onMouseOut={changeBackgroundBack}/>
                                    </div>
                                </div>

                                <div className="outer">
                                    <div className="box-cat">
                                        <img alt="lucky" src='Untitled-2.png' className="box"/>
                                    </div>
                                    <div className="box-cat">
                                        <img alt="lucky" src='Untitled-1.png' className="box onhover"/>
                                    </div>
                                </div>

                                <div className="outer">
                                    <div className="fish-cat">
                                        <img alt="lucky" src='fishtank.png' className="fish"/>
                                    </div>
                                    <div className="fish-cat">
                                        <img alt="lucky" src='fishtanklucky.png' className="fish onhover"/>
                                    </div>
                                </div>
                            </div>
                        </Box>
                        <Box gridColumn="span 8">
                            <Box sx={{display: 'grid', gridTemplateColumns: 'repeat(2, 1fr)'}}>
                                <div className="outer">
                                    <div id="cat">
                                        <div className="inner-cat">
                                            <div className={!leftHand ? `half-left-${typeOfCat}` : `half-left-down-${typeOfCat}`}></div>
                                            <div className={!rightHand ? `half-right-${typeOfCat}` : `half-right-down-${typeOfCat}`}></div>
                                        </div>
                                    </div>
                                </div>
                            </Box>


                            <div className="red"> </div>

                            <div className="center-piano">
                                <div className="piano-keys shift-right-1">
                                    <Button id={wPressed ? "key-pressed" : "key"} > {wPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {wPressed ? "C#" : "w"} </Button>
                                    <Button id={ePressed ? "key-pressed" : "key"} > {ePressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {ePressed ? "D#" : "e"} </Button>
                                    <Button id="key-hidden">  </Button>
                                    <Button id={iPressed ? "key-pressed" : "key"} > {iPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {iPressed ? "F#" : "i"} </Button>
                                    <Button id={oPressed ? "key-pressed" : "key"} > {oPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {oPressed ? "G#" : "o"} </Button>
                                    <Button id={pPressed ? "key-pressed" : "key"} > {pPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {pPressed ? "A#" : "p"} </Button>
                                </div>
                            </div>


                            <div className="center-piano">
                                <div className="piano-keys">
                                    <Button id={aPressed ? "key-pressed" : "key"} > {aPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {aPressed ? "C" : "a"} </Button>
                                    <Button id={sPressed ? "key-pressed" : "key"} > {sPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {sPressed ? "D" : "s"} </Button>
                                    <Button id={dPressed ? "key-pressed" : "key"} > {dPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {dPressed ? "E" : "d"} </Button>
                                    <Button id={fPressed ? "key-pressed" : "key"} > {fPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {fPressed ? "F" : "f"} </Button>
                                    <Button id={jPressed ? "key-pressed" : "key"} > {jPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {jPressed ? "G" : "j"} </Button>
                                    <Button id={kPressed ? "key-pressed" : "key"} > {kPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {kPressed ? "A" : "k"} </Button>
                                    <Button id={lPressed ? "key-pressed" : "key"} > {lPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {lPressed ? "B" : "l"} </Button>
                                    <Button id={cPressed ? "key-pressed" : "key"} > {cPressed ? <img id="music-note" alt="music-note" src="MusicNote.svg"/> : ""} {cPressed ? "C" : ";"} </Button>
                                </div>
                            </div>


                            <div id="spacing">
                                <MeetLuckysFriends petFinder={petfinder}> </MeetLuckysFriends>
                            </div>
                        </Box>
                    </Box>


                    <audio id="play" controls onPlay={() => {console.log("a note playing")}} autoPlay={!aPressed ? false : true} src={!aPressed ? "" : "/sounds/C5.mp3"}>
                        <source src={!aPressed? "" : "/sounds/C5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("s note playing")}} autoPlay={!sPressed ? false : true} src={!sPressed ? "" : "/sounds/D5.mp3"}>
                        <source src={!sPressed? "" : "/sounds/D5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("d note playing")}} autoPlay={!dPressed ? false : true} src={!dPressed ? "" : "/sounds/E5.mp3"}>
                        <source src={!dPressed? "" : "/sounds/E5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("d note playing")}} autoPlay={!fPressed ? false : true} src={!fPressed ? "" : "/sounds/F5.mp3"}>
                        <source src={!fPressed? "" : "/sounds/F5.mp3"} type="audio/mpeg"/>
                    </audio>

                    <audio id="play" controls onPlay={() => {console.log("j note playing")}} autoPlay={!jPressed ? false : true} src={!jPressed ? "" : "/sounds/G5.mp3"}>
                        <source src={!jPressed? "" : "/sounds/G5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("k note playing")}} autoPlay={!kPressed ? false : true} src={!kPressed ? "" : "/sounds/A5.mp3"}>
                        <source src={!kPressed? "" : "/sounds/A5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("l note playing")}} autoPlay={!lPressed ? false : true} src={!lPressed ? "" : "/sounds/B5.mp3"}>
                        <source src={!lPressed? "" : "/sounds/B5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("; note playing")}} autoPlay={!cPressed ? false : true} src={!cPressed ? "" : "/sounds/C6.mp3"}>
                        <source src={!cPressed? "" : "/sounds/C6.mp3"} type="audio/mpeg"/>
                    </audio>

                    <audio id="play" controls onPlay={() => {console.log("w note playing")}} autoPlay={!wPressed ? false : true} src={!wPressed ? "" : "/sounds/C-5.mp3"}>
                        <source src={!wPressed? "" : "/sounds/C-5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("e note playing")}} autoPlay={!ePressed ? false : true} src={!ePressed ? "" : "/sounds/D-5.mp3"}>
                        <source src={!ePressed? "" : "/sounds/D-5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("i note playing")}} autoPlay={!iPressed ? false : true} src={!iPressed ? "" : "/sounds/F-5.mp3"}>
                        <source src={!iPressed? "" : "/sounds/F-5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("o note playing")}} autoPlay={!oPressed ? false : true} src={!oPressed ? "" : "/sounds/G-5.mp3"}>
                        <source src={!oPressed? "" : "/sounds/G-5.mp3"} type="audio/mpeg"/>
                    </audio>
                    <audio id="play" controls onPlay={() => {console.log("p note playing")}} autoPlay={!pPressed ? false : true} src={!pPressed ? "" : "/sounds/A-5.mp3"}>
                        <source src={!pPressed? "" : "/sounds/A-5.mp3"} type="audio/mpeg"/>
                    </audio>

                </container>
            ): (
                <div></div>
            )}
        </div>
    );
}
export default Forecast;