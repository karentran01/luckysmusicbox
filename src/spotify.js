export const authEndPoint = `https://accounts.spotify.com/authorize`;

const redirectUri = "http://localhost:3000/"

const clientID = "edc92de4994046148cd28e26903e78aa";

const scopes = [
    "user-read-currently-playing",
    "user-read-recently-played",
    "user-read-private",
    "user-read-email",
    "playlist-modify-public",
    "playlist-modify-private",
    "user-top-read"
]

export const loginUrl = `${authEndPoint}?
client_id=${clientID}
&redirect_uri=${redirectUri}
&scope=${scopes.join("%20")}
&response_type=token
&show_dialog=true`