/*
 * Project 1 template
 * Item component JavaScript source code
 *
 * Author: Denis Gracanin
 * Version: 1.0
 */


import {BoxProps} from "@mui/material/Box";
import {Box} from "@mui/system";
import React from "react";

const Item = (props: BoxProps) => {
    const { sx, ...other } = props;
    return (
        <Box
            sx={{
                bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                boxShadow: '2px 4px 15px  rgba(182,186,195,0.25)',
                color: (theme) => (theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800'),
                border: '1px solid',
                borderColor: (theme) =>
                    theme.palette.mode === 'dark' ? 'grey.800' : 'grey.300',
                p: 1,
                m: 1,
                borderRadius: 10,
                fontSize: '0.875rem',
                fontWeight: '700',
                width: window.innerWidth/3,
                ...sx,
            }}
            {...other}
        />
    );
}

export default Item;
