import {BoxProps} from "@mui/material/Box";
import {Box} from "@mui/system";
import React from "react";
import {Button} from "@mui/material";
import "./CatCardItem.css"

const CatCardItem = (props: BoxProps) => {
    const { sx, ...other } = props;

    let x = "/images/meetluckysfriends/LuckyProPic.svg"
    if (props.profile !== null) {
        console.log(props.profile.small)
        x = props.profile.small
    }

    return (
        <Box sx={{backgroundColor: '#FFFFFF', boxShadow: '2px 4px 15px  rgba(182,186,195,0.25)', p: 2, m: 1.5, borderRadius: 3, x: 13, ...sx,
        }}{...other}>
            <div>
                <img id="cat-profile" width="200px" src={x} />
            </div>

            <div style={{display: "flex", justifyContent: "center", marginTop: 24}}>
                <h2 style={{margin: 0}}> {props.name} </h2>
                <div style={{justifyContent: "right", marginLeft: 24}}>
                    <div style={{display: "flex", justifyContent: "center"}}>
                        <p>{props.gender}</p>
                        <p> | </p>
                        <p>{props.age}</p>
                    </div>
                    <p>Animal Id: {props.animalId}</p>
                </div>
            </div>

            <div style={{marginTop: 24}}>
                <Button variant={"contained"} sx={{
                    textDecoration: "none",
                    borderRadius: 50,
                    backgroundColor: '#D69675',
                    ":hover": {
                        backgroundColor:'#6F3324'
                    }

                }
                }> <a href={props.url} style={{color: '#FFFFFF', textDecoration: "none"}}>View</a> </Button>
            </div>
        </Box>
    );
}

export default CatCardItem