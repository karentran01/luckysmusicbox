import React from 'react';
import './Weather.css'
import { Container } from "@mui/system";

function Icon({ name }) {
    let nameToLower = name.toLowerCase();
    if (nameToLower === 'thunderstorm') {
        return <img id="icon" alt="thunderstorm" src="/images/weather-icons/cloud-lighting.svg"/>
    } else if (nameToLower === 'drizzle') {
        return <img id="icon" alt="drizzle" src="/images/weather-icons/cloud-drizzle.svg"/>
    } else if (nameToLower === 'rain') {
        return <img id="icon" alt="rain" src="/images/weather-icons/cloud-rain.svg"/>
    } else if (nameToLower === 'snow') {
        return <img id="icon" alt="snow" src="/images/weather-icons/cloud-snow.svg"/>
    } else if (nameToLower === 'clear') {
        return <img id="icon" alt="clear" src="/images/weather-icons/sun.svg"/>
    } else if (nameToLower === 'clouds') {
        return <img id="icon" alt="clouds" src="/images/weather-icons/cloudy.svg"/>
    }
    return <img id="icon" alt="atmosphere" src="/images/weather-icons/atmosphere.svg"/>
}

const Weather = ({weatherData}) => (
    <Container sx={{width: '100%'}}>
        <div className="center">
            <div id="icon-div">
                <Icon
                    name={weatherData.weather[0]['main']}
                />
            </div>
            <div id="weather-data-div">
                <h1> {weatherData.weather[0]['main']} </h1>
                <p> H: {Math.round([(weatherData.main['temp_max'] * 9/5) + 32])}&deg;F L: {Math.round([(weatherData.main['temp_min'] * 9/5) + 32])}&deg;F </p>
                <p id="loc"> My Location: {weatherData.name} </p>
            </div>
        </div>
    </Container>

)
export default Weather;