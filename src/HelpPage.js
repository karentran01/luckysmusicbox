import React from "react";
import {Container} from "@mui/system";
import "./HelpPage.css"
import {Box} from "@mui/system";
import {loginUrl} from "./spotify";

function HelpPage() {

    return (
        <Container className="App">
            <div id="help-div">
                <h2 id="help-title"> What is Lucky's Music Box </h2>
                <p id="help-p"> Lucky’s Music Box is a web application artifact that combines computers, cats, and music. The web application has three features: a curated Spotify playlist generator, a piano keyboard with cat meows, and an area to “meet Lucky’s friends, which lists cats available for adoption around the user’s location. </p>
                <p id="help-p"> The curated Spotify playlist generates a playlist based on the user’s current weather conditions. To further personalize each playlist to the user, the user’s top artist or preferred artist, the user’s top song, and the seed genres associated with the current weather condition also play a part in crafting a unique playlist for each user. </p>
                <p id="help-p"> The piano keyboard with cat meows combines the love for music and cats. The piano has 13 “keys” controlled through the keys on a computer’s keyboard. The meows are recorded from real cats and featured in the “meet Lucky’s friends” area. </p>
                <p id="help-p"> Lucky’s music box was developed to explore the theme of computer music but also to encourage cat and music lovers to have fun. </p>

                <Box sx={{display: 'grid', gridTemplateColumns: 'repeat(3, 1fr)'}}>
                    <div id="help-div"> <img id="help-cats" src="LuckyBlack.svg" alt="the-cats"/> <p id="help-p"> The black cat is present when the weather is thunderstorm, drizzle, and rain represented by its rainy outfit. </p> </div>
                    <div id="help-div"> <img id="help-cats" src="LuckyOrange.svg" alt="the-cats"/> <p id="help-p">  The orange cat is present when the weather is cloudy and clear represented by its tropical outfit. </p> </div>
                    <div id="help-div"> <img id="help-cats" src="LuckyTan.svg" alt="the-cats"/> <p id="help-p">  The tan cat is presented when the weather is atmosphere represented by its casual outfit. </p> </div>
                </Box>
            </div>

        </Container>
    )
}

export default HelpPage