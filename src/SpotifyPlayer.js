import React from 'react';
import SpotifyPlayer from 'react-spotify-player'; //https://www.npmjs.com/package/react-spotify-player

const size = {
    width: '100%',
    height: window.innerHeight/3 - 80,
};
const view = 'list'; // or 'coverart'
const theme = 'black'; // or 'white'

const SpotifyPlayerComp = ({playlist_id}) => {

    return (

            <SpotifyPlayer
                uri={"spotify:playlist:" + playlist_id}
                size={size}
                view={view}
                theme={theme}
            />
    );
}

export default SpotifyPlayerComp;
