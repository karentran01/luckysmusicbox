/**
 * Spotify Authentication Video: https://www.youtube.com/watch?v=wBq3HCvYfUg&t=880s
 *
 * Part of the video helped us to retrieve the token.
 */

import React, {useEffect, useState} from "react";
import './App.css';
import SpotifyWebApi from 'spotify-web-api-js'
import Forecast from "./Forecast";
import Welcome from "./Welcome"

export const getTokenFromUrl = () => {
    // retrieves access token from the redirect url
    return window.location.hash
        .substring(1)
        .split('&')
        .reduce((initial, item) => {
            let parts = item.split('=');
            initial[parts[0]] = decodeURI(parts[1])
            return initial;
        },{});
}

function App() {

    const [spotifyToken, setSpotifyToken] = useState(sessionStorage.getItem('spotify_token'))

     // Store the access token so it can be used within the session
    useEffect(() => {
        const spotify = new SpotifyWebApi();
        const _spotifyToken = getTokenFromUrl().access_token;

        window.location.hash = "";
        if (_spotifyToken) {
            setSpotifyToken(_spotifyToken)
            sessionStorage.setItem('spotify_token', _spotifyToken);
            spotify.getMe().then((user) => {
            });
        }

    }, [])

    // Conditional rendering: if there is no spotify access token, the user must login, else they can access lucky's music box
    return (
        <div className="App">
            <header className="App-header">
                {!spotifyToken ?
                    <Welcome></Welcome>
                    : <div></div>
                }

                {spotifyToken ?
                    <Forecast spotify_token={spotifyToken}></Forecast>
                    : <div></div>
                }
            </header>
        </div>
    );
}

export default App;
