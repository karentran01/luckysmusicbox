import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.css';
import App from './App';
import MeetLuckysFriends from "./MeetLuckysFriends";
import FriendDetailPage from "./FriendDetailPage";
import HelpPage from "./HelpPage";

export default function Root() {
    return (
        <BrowserRouter>
            <Routes>
                <Route>
                    <Route index element={<App />} />
                    <Route path="meet-luckys-friends" element={<MeetLuckysFriends/>}/>
                    <Route path="friend-detail-pages" element={<FriendDetailPage/>}/>
                    <Route path="help" element={<HelpPage/>}/>
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<Root />);
